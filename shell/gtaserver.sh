#!/bin/bash
export RWMP_PATH=..

server_start() {
cd $RWMP_PATH
cat ./server/server_log.txt >> ./server/full_server_log.txt
rm ./server/server_log.txt
cd server
./rwmp-server &
exit 0
}

server_stop() {
killall startgtaserver.sh
killall rwmp-server
exit 0
}
case "$1" in
'start')
server_start
;;
'stop')
server_stop
;;
'restart')
server_restart
;;
*)
echo "usage $0 start|stop|restart"
esac
exit 0
