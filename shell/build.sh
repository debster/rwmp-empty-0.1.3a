#!/bin/bash

pawncc "-;+" "-(+" "-i../source/include" ../source/gamemode.pwn

if [ $(stat -c%s "gamemode.amx") -gt 0 ];
then
	mv gamemode.amx ../server/gamemodes/
else
	rm gamemode.amx
fi
